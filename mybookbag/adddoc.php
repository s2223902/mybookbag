<?php //Page to initiate the adding of documents.
	ob_start();
    session_start();
	$_SESSION["currtype"] = "b";
	include('functions.php');
    if(!isset($_SESSION['username'])){
         header("Location: index.php");
    }

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<script type="text/javascript" src="js/Placeholders.js"></script>
<script type="text/javascript">
	Placeholders.init({
    live: true,
    hideOnFocus: true});
</script>
<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>Add a new MyDoc</title>
</head>
<body>
<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
	<div id="carbonForm2">
	<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
		<ul id="nav">
	<li><a href="page.php">Home</a></li>
	<li class="current"><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (<?php echo checkMessages();?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
	<p><h1>My Books</h2></p>
<?php if(isset($_SESSION["Message"])){
echo "<p>{$_SESSION["Message"]}</p>";
unset($_SESSION["Message"]);
}
?>
<form action="check.php" method="post">
<h3>Insert all the data you can, then click on "Send".</h3>
<table id="searchtable">
<!-- Creates the initial form that will be used to check if there aren't already any matches in the catalog -->
<tr><td>Title*</td><td><input name="title" type="text"></td></tr>
<tr><td>Author</td><td><input name="author" type="text"></td></tr>
<tr><td>Genre</td><td><input name="genre" type="text"></td></tr>
<tr><td>Publisher</td><td><input name="publisher" type="text"></td></tr>
<tr><td>ISBN</td><td><input name="isbn" type="text"></td></tr>
<tr><td>Type</td><td><select name="type">
<option value="b">Book</option>
<option value="e">E-book</option>
<option value="j">Journal</option>
</select></td></tr>
* Fields with * need to be inserted at least.
<tr><td></td><td><input type="submit" name="submit"></td>
</table>
</form>


	</div>
	</div>
	</div>

</body>
</html>

