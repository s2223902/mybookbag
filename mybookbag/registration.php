<?php

error_reporting(E_ALL);

include ('database_connection.php');
if (isset($_POST['formsubmitted'])) {
    $error = array();//Declare An Array to store any error message  
    if (empty($_POST['name'])) {//if no name has been supplied 
        $error[] = 'Please Enter a username ';//add to array "error"
    } else {
        $name = $_POST['name'];//else assign it a variable
    }
	

    if (empty($_POST['e-mail'])) {
        $error[] = 'Please Enter your Email ';
    } else {


        if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_POST['e-mail'])) {
           //regular expression for email validation
            $Email = $_POST['e-mail'];
        } else {
             $error[] = 'Your EMail Address is invalid  ';
        }


    }


    if (empty($_POST['Password'])) {
        $error[] = 'Please Enter Your Password ';
    } else {
        $Password = $_POST['Password'];
    }


    if (empty($error)) //send to Database if there's no error '

    { // If everything's OK...

        // Make sure the email address is available:
        $query_verify_email = "SELECT * FROM users  WHERE email ='$Email'";
        $result_verify_email = mysqli_query($dbc, $query_verify_email);
        if (!$result_verify_email) {//if the Query Failed ,similar to if($result_verify_email==false)
            echo ' Database Error Occured ';
        }

        if (mysqli_num_rows($result_verify_email) == 0) { // IF no previous user is using this email .


            // Create a unique  activation code:
            $activation = md5(uniqid(rand(), true));
			// Create MD5 hashing password
			$PasswordHash = md5($Password);


            $query_insert_user = "INSERT INTO `users` (`username`,`email`, `password`, `activation`) VALUES ( '$name', '$Email', '$PasswordHash', '$activation')";


            $result_insert_user = mysqli_query($dbc, $query_insert_user)  or die("MySQL ERROR: it failed dumbass".mysql_error());
            if (!$result_insert_user) {
                echo 'Query Failed ';
            }

            if (mysqli_affected_rows($dbc) == 1) { //If the Insert Query was successfull.


                // Send the email:
                $message = " Thank you for registering at MyBookBag.com! \n\n To activate your account, please click on this link:\n\n";
                $message .= WEBSITE_URL . '/activate.php?email=' . urlencode($Email) . "&key=$activation";
                mail($Email, 'Registration Confirmation', $message, 'From: registration@mybookbag.com');

                // Flush the buffered output.


                // Finish the page:
                echo header("Location: confirmation.php");
							exit;


            } else { // If it did not run OK.
                echo '<div class="errormsgbox">You could not be registered due to a system
error. We apologize for any
inconvenience.</div>';
            }

        } else { // The email address is not available.
            echo '<div class="errormsgbox" >That email
address has already been registered.
</div>';
        }

    } else {//If the "error" array contains error msg , display them
        
        

echo '<div class="errormsgbox"> <ol>';
        foreach ($error as $key => $values) {
            
            echo '	<li>'.$values.'</li>';


       
        }
        echo '</ol></div>';

    }
  
    mysqli_close($dbc);//Close the DB Connection

} // End of the main Submit conditional.



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<script type="text/javascript" src="js/Placeholders.js"></script>
<script type="text/javascript">
	Placeholders.init({
    live: true,
    hideOnFocus: true});
</script>
<title>MyBookBag Registration Form</title>
</head>
<body>

<?php include ("php/random-bg.php"); ?>

<form action="registration.php" method="post" class="registration_form">
<div id="carbonForm">
	<div id="logo">
		<img src="img/logo.png" />
	</div>
	
	<h1>Signup</h1>

    <form action="registration.php" method="post" id="signupForm">

    <div class="fieldContainer">
		<p><span style="margin: 0 0 0 10;">Create a new account</span></p>
		<p><span style="margin: 0 0 0 10;">Already a member? <a href="index.php">Log in</a></span></p>
		<br />
		
        <div class="formRow">
            <div class="field">
                <input type="text" name="name" id="name" placeholder="Username" />
            </div>
        </div>
       
        
        <div class="formRow">
           <div class="field">
                <input type="text" name="e-mail" id="e-mail" placeholder="email address" />
            </div>
			
        </div>
		   <div class="formRow">
            <div class="field">
                <input type="password" name="Password" id="Password" placeholder="password" />
            </div>
        </div>
        
        
    </div> <!-- Closing fieldContainer -->
    
   
	<div class="signupButton">
        <input type="submit" name="formsubmitted" id="submit" value="Register" />
    </div>
        </div>
    
    </form>
</body>
</html>
