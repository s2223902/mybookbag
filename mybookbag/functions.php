<?php
include('db_connection.php');
function checkMessages(){ //Checks if there are any new messages or not, and returns the amount.

$checkmsg = mysql_query('select m1.id, m1.title, m1.timestamp, count(m2.id) as reps, users.id as userid, users.username from pm as m1, pm as m2,users where ((m1.user1="'.$_SESSION['id'].'" and m1.user1read="no" and users.id=m1.user2) or (m1.user2="'.$_SESSION['id'].'" and m1.user2read="no" and users.id=m1.user1)) and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc') or die ("fetch1:".mysql_error());
$amountmsg = mysql_num_rows($checkmsg);
return $amountmsg;
}

function books_Set($type){ //Checks how many books have been set with the given type for the user
$query = "select `user_book`.`entryid` from `book` natural join `user_book` where id={$_SESSION["id"]} and `book`.`type`='{$type}'";
$handle = mysql_query($query) or die(mysql_error());
$amount = intval(mysql_num_rows($handle));
return $amount;
}

function loadBook($type){ //Loads the books of the given type for the user.
$id=$_SESSION["id"];
if(isset($_SESSION["remoteid"])){ //If it tries to load books from a different user.
$addpermission = "and permission=1";
$id = $_SESSION["remoteid"];
}
else{
$addpermission = "";
}

$orderBy = array('title','author','isbn');
if (isset($_GET['order']) && in_array($_GET['order'], $orderBy)) {
    $order = $_GET['order'];
}
else{
	$order = 'title asc';
}
if($order==$_SESSION["Order"] && $_GET["order"]){
	$order = "$order desc";
	}
$_SESSION["Order"] = $order;

		$query = "select `title`,`author`,`isbn` from `book` natural join `author_book`,`user_book` where `author_book`.`entryid`=`user_book`.`entryid` and `user_book`.`entryid`=`book`.`entryid` and `user_book`.`id`=$id and type='{$type}' $addpermission order by $order";
		//echo $query;

	$handle = mysql_query($query) or die(mysql_error());
	$nr = 0;
	while($row = mysql_fetch_array($handle)){
		$userbooks[$nr]["title"] = $row["title"];
		$userbooks[$nr]["author"] = $row["author"];
		$userbooks[$nr]["isbn"] = $row["isbn"];
		$nr++;
		}
		$_SESSION["userbooks"] = $userbooks;
		return $userbooks;
}

function amount_Type(){	//Checks if there are any books at all with the given type, and returns False, or the corresponding number.
$type = $_SESSION["currtype"];
$query = "select `user_book`.`entryid` from `book` natural join `user_book` where id={$_SESSION["id"]} and `book`.`type`='{$type}'";
$handle = mysql_query($query) or die(mysql_error());
$amount = intval(mysql_num_rows($handle));
if($amount==0){$amount=False;}
return $amount;
}


function showBooks($userbooks){ //Prints all the books for the user page
if(amount_Type()){
	$Table = "<table><tr><td><b><a href=\"?order=title\">Title</a></b></td><td><b><a href=\"?order=author\">Author</a></b></td><td><b><a href=\"?order=isbn\">ISBn</a></b></td></tr>";
	for($i=1;$i<count($userbooks);$i++){
	$title = $userbooks[$i]["title"];
	$author = $userbooks[$i]["author"];
	$isbn = $userbooks[$i]["isbn"];
	$Table = "$Table<tr><td>$title</td><td>$author</td><td>$isbn</td></tr>";
	}
	$Table ="$Table</table>";
	return $Table;
	}
	else{
	return "You have not added any books yet. Please view the MyBookBag tab and add some books so we can display them for you here.";
	}
	}
/////
function stripArray($arr){ //Strips the array and creates session variables of it
	$Keys = array_keys($arr);
	foreach($Keys as $Key){
		$_SESSION["Create"][$Key] = $arr[$Key];
		}
	if(!isset($arr["author"])){
		$_SESSION["Create"]["author"] = False;
		}
	if(!isset($arr["title"])){
		$_SESSION["Create"]["title"] = False;
		}
	//else{
		//$_SESSION["Create"]["title"] = $arr["title"];
		//}
	return;
	}

	
function isAccepted($form){ //Checks if the inserted form will be accepted. A title is the minimum.
if (isset($_GET['order']) && isset($_SESSION["results"])){
return True;
}
 $accepted = True;
 if(isset($_GET["sort"])){
 return $accepted;
 }
	
	if(empty($form["title"]) && empty($form["author"])){
		$accepted = False;
		
	}
	else if ($form["title"]){
	$list = array("of","and","in","in the","against","against the","for","to","of the","to the","and the","with","with the","by","by the","because of","vs.","for the");
	foreach($list as $word){
	if($form["title"]==$word){
	$accepted = False;
	}
	}	
		return $accepted;
}
}

function loadFriends(){ //Loads all user's friends
$query = "select `friend_id`,`p_id`,`username`,`email` from `users` natural join `friend_of` where `friend_of`.`friend_id`=`users`.`id` and `p_id`={$_SESSION["id"]} and `friend_of`.`accepted`=2";
$handle = mysql_query($query);
$query2 = "select `p_id`,`friend_id`,`username`,`email` from `users` natural join `friend_of` where `friend_of`.`p_id`=`users`.`id` and `friend_id`={$_SESSION["id"]} and `friend_of`.`accepted`=2"; //For reverse cases! difference between the 'adder and the added' 
$handle2 = mysql_query($query2) or die(mysql_error());
if(mysql_num_rows($handle)==0 && mysql_num_rows($handle2)==0 ){
return False;
}
$friendTable = "<table><form action=\"remoteuser.php\" method=\"post\" name=\"form1\"><tr><td>Username</td><td>E-Mail</td></tr>";
while($row=mysql_fetch_assoc($handle)){ //Collects all the results it can find.
$username = $row["username"];
$email = $row["email"];
$id = $row["friend_id"];
$friendTable = "$friendTable<tr><td><a href=\"remoteuser.php?id=$id\">$username</a></td><td>$email</td></tr>";
}

while($row=mysql_fetch_assoc($handle2)){
$username = $row["username"];
$email = $row["email"];
$id = $row["p_id"];
$friendTable = "$friendTable<tr><td><a href=\"remoteuser.php?id=$id\">$username</a></td></td><td>$email</td></tr>";
}
$friendTable = "$friendTable</table>";
return $friendTable;
}

function getOutPending(){ //Retrieves outgoing friendrequests.
$query = "SELECT  `friend_id` 
FROM  `friend_of` 
WHERE  `p_id` ={$_SESSION["id"]}
AND  `friend_of`.`accepted` =1
LIMIT 0 , 30";
$handle = mysql_query($query);
$pending = mysql_num_rows($handle);
if($pending>0){
echo $pending;
}
else{
echo 0;
}
}

function getInPending(){ //Retrieves incoming friendrequests.
$query = "SELECT  `p_id` 
FROM  `friend_of` 
WHERE  `friend_id` ={$_SESSION["id"]}
AND  `friend_of`.`accepted` =1
LIMIT 0 , 30";
$handle = mysql_query($query);
$pending = mysql_num_rows($handle);
if($pending>0){
echo $pending;
}
else{
echo 0;
}
}

function showFriends(){ //Prints all user's friends
if(!$_SESSION["friends"]){
echo "<p>You currently have no friends! View the MyFriends tab to add some people you know!</p>";
}
else{ 
echo "<p>These are your friends!</p>";
echo $_SESSION["friends"];
}
}

function requestForm(){ //Retrieves if any friendship requests need answering.
$query = "
select `username`,`users`.`id`
from `users` 
natural join 
`friend_of` 
where `friend_of`.`friend_id`={$_SESSION["id"]} 
and `p_id`=`users`.`id` 
and `friend_of`.`accepted`=1";
$handle = mysql_query($query) or die(mysql_error());
if(mysql_num_rows($handle)==0){
return "You currently have no requests that require answering.";
}

$requestForm = "<table><tr><td>Username</td></tr>";
while($row = mysql_fetch_array($handle)){
$request = $row["username"];
$id = $row["id"];
$requestForm = "<form action=\"confirmrequest.php\" method=\"post\">$requestForm<tr><td>$request</td><td><input type=\"submit\" name=\"Answer\" value=\"Accept\" ></input></td><td><input type=\"submit\" name=\"Answer\" value=\"Reject\" ></input></td><input type=\"hidden\" name=\"targetid\" value=\"{$id}\"></input></tr></form>";
}
return $requestForm;
}

function showRequests(){ //Prints all requests
return $_SESSION["requests"];
}

function sendAnswer(){ //Sends the answer (Accept/Reject) and creates additional rows corresponding to that.
if($_POST["Answer"]=="Accept"){
$query = "update `friend_of` set `accepted`=2 where `p_id`={$_POST["targetid"]} and `friend_id`={$_SESSION["id"]} and `friend_of`.`accepted`=1";
$query2 = "delete from `friend_of` where `p_id`={$_POST["targetid"]} and `friend_id`={$_SESSION["id"]} and `friend_of`.`accepted`=0";
mysql_query($query) or die(mysql_error());
mysql_query($query2) or die(mysql_error());
$_SESSION["notify"] = "You have accepted the friendrequest! Your friend has now newly appeared in your friendslist! Check it out!";
}
else{
$query = "update `friend_of` set `accepted`=0 where `p_id`={$_POST["targetid"]} and `friend_id`={$_SESSION["id"]} and `friend_of`.`accepted`=1";
mysql_query($query) or die(mysql_error());
$_SESSION["notify"] = "You rejected the friendrequest! Boo-hoo you complex life-form with emotional issues!";
}
}

function addFriendForm(){ //Requests friendship form
$form = "<form action=\"processrequest.php\" method=\"post\">
<table><tr><td><input type=\"text\" name=\"username\" value=\"username\"></input></td><td><input type=\"text\" name=\"email\" value=\"E-mail\"></input></td><td><input type=\"submit\" name=\"submit\" value=\"Send Request!\"></input></td></tr></table>";
return $form;
}

function determineTarget($username,$email){ //Gets the userid from inserted username and/or e-mail
if(empty($username)){
$query = "select `id` from `users` where `email`='{$email}'";
}
if(empty($email)){
$query = "select `id` from `users` where `username`='{$username}'";
}
if($username&&$email){
$query = "select `id` from `users` where `email`='{$email}' or `username`='{$username}'";
}

$handle = mysql_query($query) or die(mysql_error());
while($row = mysql_fetch_array($handle)){
$id = $row["id"];
}
return $id;
}

function loadRemoteLibrary($called_id){ // Loads the books that can be seen by the current user FROM another user... So : Public books and friend books (if they are friends)
$totalRemoteLib = array();

if(friends($called_id)){
$totalRemoteLib[]= loadRemoteBook("b",1,$called_id);
$totalRemoteLib[]= loadRemoteBook("e",1,$called_id);
$totalRemoteLib[]= loadRemoteBook("j",1,$called_id);

}
$totalRemoteLib[]= loadRemoteBook("b",2,$called_id);
$totalRemoteLib[]= loadRemoteBook("e",2,$called_id);
$totalRemoteLib[]= loadRemoteBook("j",2,$called_id);
$RemoteTable = array();
foreach($totalRemoteLib as $piece){
$Table = "<table><tr><td><b>Title</b></td><td><b>Author</b></td><td><b>ISBn</b></td><td>Type</td></tr>";
for($i=1;$i<count($piece);$i++){
$title = $piece[$i]["title"];
$author = $piece[$i]["author"];
$isbn = $piece[$i]["isbn"];

$Table = "$Table<tr><td>$title</td><td>$author</td><td>$isbn</td></tr>";
}
$Table ="$Table</table>";
$RemoteTable[$i] = $Table;
}
return $RemoteTable;
}

function loadRemoteBook($type,$permission,$id){
		$query = "select `title`,`author`,`isbn` from `book` natural join `author_book`,`user_book` where `author_book`.`entryid`=`user_book`.`entryid` and `user_book`.`entryid`=`book`.`entryid` and `user_book`.`id`=$id and type='{$type}' and permission=$permission order by title" or die("?");

	$handle = mysql_query($query) or die(mysql_error());
	$nr = 0;
	while($row = mysql_fetch_array($handle)){
		$userbooks[$nr]["title"] = $row["title"];
		$userbooks[$nr]["author"] = $row["author"];
		$userbooks[$nr]["isbn"] = $row["isbn"];
		$nr++;
		}
		$_SESSION["userbooks"] = $userbooks;
		return $userbooks;
}

function friends($called_id){
$query = "select `p_id` from `users` natural join `friend_of` where `friend_of`.`friend_id`={$called_id} and `p_id`={$_SESSION["id"]} and `friend_of`.`accepted`=2";
$handle = mysql_query($query);
$query2 = "select `friend_id` from `users` natural join `friend_of` where `friend_of`.`p_id`={$called_id} and `friend_id`={$_SESSION["id"]} and `friend_of`.`accepted`=2"; //For reverse cases! difference between the 'adder and the added' 
$handle2 = mysql_query($query2) or die(mysql_error());
if(mysql_num_rows($handle)==0 && mysql_num_rows($handle2)==0 ){
return False;
}
else{
return True;
}
}



function getRemoteName($id){
$query = "select username from `users` where `id`=$id";
$handle = mysql_query($query) or die(mysql_error());
while($row = mysql_fetch_array($handle)){
$name = $row["username"];
}
return $name;
}

function getid(){
	//Selects the highest ID in the database so new entries can be submitted accordingly
	$IDQ = "select max(entryid) from entry";
	$handle = mysql_query($IDQ) or die(mysql_error());
	while($row = mysql_fetch_array($handle)){
		$id = $row["max(entryid)"];
		}
		return $id;
	}



?>