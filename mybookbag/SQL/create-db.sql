create table entry(entryid int(11) auto_increment, type varchar(25), primary key(entryid));
create table book(title varchar(65), entryid int(11), isbn varchar(13) null, type varchar(1), primary key(entryid,isbn), foreign key(entryid) references entry(entryid));
create table author(name varchar(45),birthyear varchar(4),deathyear varchar(4) null, primary key(name,birthyear));
create table author_book(author varchar(45),entryid int(11), primary key(author,entryid), foreign key(entryid) references entry(entryid), foreign key(author) references author(name));
create table publisher(name varchar(65), primary key(name));
create table publisher_book(publisher varchar(65), entryid int(11), primary key(publisher,entryid), foreign key(publisher) references publisher(name), foreign key(entryid) references entry(entryid));
create table genre_book(genre varchar(20),entryid int(11), primary key(genre,entryid), foreign key(entryid) references entry(entryid));

create table user(id int(10) AUTO_INCREMENT, username varchar(50) unique, email varchar(40) unique, password varchar(25) not null, activation varchar(250) null, primary key(id));

create table friend_of(p_id int(10), friend_id int(10), accepted int(1) default 0, foreign key(p_id) references user(id), foreign key(friend_id) references user(id), primary key(p_id,friend_id));

create table user_book(id int(10), entryid int(11), permission int(1) default 1, primary key(id,entryid), foreign key(id) references user(id), foreign key(entryid) references entry(entryid));

create table loans_book(id int(10), entryid int(11), id2 int(10), foreign key(entryid) references entry(entryid), foreign key(id) references user(id), foreign key(id2) references user(id), primary key(id,entryid,id2));

insert into user(id,username,email,password,activation) values(null, 'Admin','admin\@mybookbag.com','password',null);

