#Library to spit out MySQL statements
#Lennart Kloppenburg 2012
#There are probably ways to do this a lot faster than the way I did it. But this seemed easy enough :))
#I guess if you want to use this yourself, you need to add additional classes / attributes and change some SQLWrite methods,
#because this way it only works with our own database design.
#A heads-up, creating 50000 books will create 272500~ statements... SQL will need some time :D
#All titles and authors are fake, or extremely coincidentally real.

from random import *
class Book:
    #Makes the book class
    def __init__(self):
        self.title = ""
        self.genre = ""
        self.year = randint(1990,2010)
        self.pages = randint(50,1000)
        self.publisher = ""
        self.isbn = ""
        self.id = ""
        self.author = ""
        self.type = ""

    def setType(self,Type):
        self.type = Type

    def setTitle(self,Title):
        self.title = Title

    def setID(self,ID):
        self.id = ID

    def SQLBook(self):
        return "INSERT INTO `book`(`title`,`entryid`,`isbn`,`type`) VALUES('{0}','{1}','{2}','{3}');".format(self.title,self.id,self.isbn,self.type)

    def SQLGenre_Book(self):
        return "INSERT INTO `genre_book`(`genre`, `entryid`) VALUES ('{0}','{1}');".format(self.genre,self.id)
    #For the relations
    def setAuthor(self,Author): 
        self.author = Author

    def setGenre(self,Genre):
        self.genre = Genre

    def setYear(self,Year):
        self.year = Year

    def setPublisher(self,Publisher):
        self.publisher = Publisher

    def setISBN(self,ISBN):
        self.isbn = ISBN
    #For debugging... Shows if the book was properly added.
    def describe(self):
        "The book \"{0}\" is written by {1} and is published by {2}. The ISBN is {3} and the genre is {4}. It has {5} pages and it was published in {6}.".format(self.title,self.author,self.publisher,self.isbn,self.genre,self.pages,self.year)

class Entry:
    #Creates entry class
    def __init__(self):
        self.id = ""

    def setID(self,ID):
        self.id = ID

    def SQLEntry(self):
        return "INSERT INTO `entry`(`entryid`,`type`) VALUES('{0}','b');".format(self.id)

class Author:
    #Creates author class
    def __init__(self):
        self.name = ""
        self.birthyear = randint(1880,1980)
        self.deathyear = self.birthyear + randint(20,100)

    def setName(self,Name):
        self.name = Name

    def SQLAuthor(self):
        return "INSERT INTO `author`(`name`,`birthyear`,`deathyear`) VALUES('{0}','{1}','{2}');".format(self.name,self.birthyear,self.deathyear)
        
    def SQLAuthor_Book(self,entryid):
        return "INSERT INTO `author_book`(`author`, `entryid`) VALUES ('{0}','{1}');".format(self.name,entryid)

class Publisher:
    #Creates Publisher class
    def __init__(self):
        self.name = ""

    def setName(self,Name):
        self.name = Name

    def SQLPublisher(self):
        return "INSERT INTO `publisher`(`name`) VALUES ('{0}');".format(self.name)

    def SQLPublisher_Book(self,entryid):
        return "INSERT INTO `publisher_book`(`publisher`, `entryid`) VALUES ('{0}','{1}');".format(self.name,entryid)

def Bookbuilder(Amount,Nouns,Adjectives):
    #Builds the book titles. It randomly (randIndex()) picks nouns from the textfile and connects them using "stickies" and optionally an adjective. Then returns the list
    Stickies = ["of","and","in","in the","against","against the","for","to","of the","to the","and the","with","with the","by","by the","because of","vs.","for the"]
    Books = []
    for i in range(Amount):
        _0 = randIndex(Nouns)
        Noun = Nouns[_0].capitalize()
        _1 = randIndex(Stickies)
        Sticky = Stickies[_1]
        _2 = randIndex(Nouns)
        Noun2 = Nouns[_2].capitalize()     
        if random()>0.5:
            Adj_ind = randIndex(Adjectives)
            Adjective = Adjectives[Adj_ind]
        else:
            Adjective = None
        Books.append(writeTitle(Noun,Sticky,Adjective,Noun2))
    return Books

def Authorbuilder(Amount,firstnames,surnames):
    #Builds the author names. It randomly picks a first and last name from the text files and concatenates them. Then returns the list.
    Authors = []
    for i in range(Amount):
        Firstname_ind = randIndex(firstnames)
        Surname_ind = randIndex(surnames)
        Firstname = firstnames[Firstname_ind]
        Surname = surnames[Surname_ind]
        Author = writeAuthor(Firstname,Surname)
        if Author not in Authors:
            Authors.append(Author)
    return Authors

def Publisherbuilder(Amount,surnames):
    #Builds the publishers. It randomly picks a last name and concatenates it with random company suffixes.
    Publishers = []
    Addsels = ["Inc.","Cooperations.","Publishers."]
    for i in range(Amount):
        Addsels_ind = randIndex(Addsels)
        Surname_ind = randIndex(surnames)
        Publisher = writePublisher(Addsels[Addsels_ind],surnames[Surname_ind])
        if Publisher not in Publishers:
            Publishers.append(Publisher)
    return Publishers
    
def ISBNbuilder(Amount):
    #Builds the ISBN that will be used for the book. It simply iterates [Amount] times and concatenates random numbers together, starting with 0 or 9.
    ISBNs = []
    Numbers = "0123456789"
    for i in range(Amount):
        ISBN = ""
        for A in range(13):
            I = randint(0,9)
            if A==0:
                B = random()
                if B>0.5:
                    I = 0
                if B<=0.5:
                    I = 9          
            else:
                I = randint(0,9)
            ISBN = "{0}{1}".format(ISBN,I)
        if ISBN not in ISBNs:
            ISBNs.append(ISBN)
    return ISBNs

def createBookObject(ISBN,Bewk,Genre,Publisher,ID,Author,Type):
    #Takes book attributes as parameter, sets them, then returns the book object.
    book = Book()
    book.setISBN(ISBN)
    book.setTitle(Bewk)
    book.setGenre(Genre)
    book.setPublisher(Publisher)
    book.setID(ID)
    book.setAuthor(Author)
    book.setType(Type)
    return book

def createAuthorObject(Name):
    #Takes author attributes as parameter, sets them, then returns the author object.
    author = Author()
    author.setName(Name)
    return author

def createPublisherObject(Name):
    #Takes publisher attributes as parameter, sets them, then returns the publisher object.
    publisher = Publisher()
    publisher.setName(Name)
    return publisher

def writeTitle(_0,_1,_2,_3):
    #Concatenates the parameters (provided they exist) together to form the title. Then returns a function call to "Filter()", which adds escape characters to the title for MySQL comfort.
    if _2 == None:
        Title = "{0} {1} {2}".format(_0,_1,_3)
    else:
        Title = "{0} {1} {2} {3}".format(_0,_1,_2,_3)    
    return Filter(Title)

def writeAuthor(Firstname,Surname):
    #Concatenates the parameters to form the author's name
    return "{0} {1}".format(Firstname,Surname)

def writePublisher(Addsel,Surname):
    #Concatenates the parameters to form the publisher's name
    return "{0} {1}".format(Surname,Addsel)

##def makeTitles(Amount,Nouns,Adjectives,Stickies):
##    Titles = []
##    for i in range(Amount):
##        Titles.append(writeTitle(Noun,Sticky,Adjective,Noun2))
##    return Titles

def randIndex(List):
    #Using the length of the list, returns a random index of it
    return (int(round((random() * len(List)),0))-1)

def Filter(Str):
    #Adds MySQL-friendly escape characters to the string.
    return "\\'".join(Str.split("'"))

def Bibuilder(Amount,Nouns,Adjectives,firstnames,surnames,ID):
    Books = Bookbuilder(Amount,Nouns,Adjectives)
    Authors = Authorbuilder(int(Amount*0.35),firstnames,surnames)

    Publishers = Publisherbuilder(int(Amount*0.10),surnames)
    ISBNs = ISBNbuilder(Amount)
    Library = []
    EntryLibrary = []
    Chosen_Authors = []
    AuthorNames = []
    PublisherLibrary = []
    PublisherNames = []
    Genres = ["Thriller","Adventure","Sci-Fi","Horror","Romance","Science","Education"]
    Types = ["b","j","e"]
    Thrillers = 0
    for Bewk in Books:
        Entray = Entry()
        
        Genre_ind = randIndex(Genres)
        Genre = Genres[Genre_ind]
        
        Publish_ind = randIndex(Publishers)
        Publisher = Publishers[Publish_ind]
        
        ISBN_ind = randIndex(ISBNs)
        ISBN = ISBNs[ISBN_ind]
        ISBNs.remove(ISBN)
        
        Author_ind = randIndex(Authors)
        Author = Authors[Author_ind]

        Type_ind = randIndex(Types)
        Type = Types[Type_ind]
        
        a = createBookObject(ISBN,Bewk,Genre,Publisher,ID,Author,Type)
        b = createAuthorObject(Author)
        c = createPublisherObject(Publisher)
        Library.append(a)
        if b.name not in AuthorNames:
            Chosen_Authors.append(b)
            AuthorNames.append(b.name)
        Entray.setID(ID)
        ID+=1
        EntryLibrary.append(Entray)
        if c.name not in PublisherNames:
            PublisherLibrary.append(c)
            PublisherNames.append(c.name)
        
    FileName = input("Please name the file for your statements. It will have the extension \".sql\".\n")
    file = open("../Data/{0}.sql".format(FileName), 'w+')
    for Books in Library:
        SQL = ""
        for Entries in EntryLibrary:
            if Entries.id == Books.id:
                file.write("{0}\n".format(Entries.SQLEntry()))                
        file.write("{0}\n".format(Books.SQLBook()))
        file.write("{0}\n".format(Books.SQLGenre_Book()))
        for Authorz in Chosen_Authors:            
            if Authorz.name == Books.author:
                if Authorz.name in AuthorNames:
                    file.write("{0}\n".format(Authorz.SQLAuthor()))
                    AuthorNames.remove(Authorz.name)
                file.write("{0}\n".format(Authorz.SQLAuthor_Book(Books.id)))

        for Publisherz in PublisherLibrary:
            if Publisherz.name == Books.publisher:
                if Publisherz.name in PublisherNames:
                    file.write("{0}\n".format(Publisherz.SQLPublisher()))
                    PublisherNames.remove(Publisherz.name)
                file.write("{0}\n".format(Publisherz.SQLPublisher_Book(Books.id)))
    Lines = int(Amount*5.45)            
    print("Approximately {0} lines have been written.\n{1} book rows were written.\n{2} author rows were written, {1} author/book associations were written.\n{3} publisher rows were written, {1} publisher/book associations were written.\n{1} genre/book associations were written.".format(Lines,Amount,int(Amount*0.35),int(Amount*0.10)))
    file.close()
    print("Your data has been saved successfully under the name \"{0}.sql\"., in the map \"Data\".\nPlease quit the program now and insert your data.".format(FileName))
