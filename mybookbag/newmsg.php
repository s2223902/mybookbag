<?php
session_start();
include('db_connection.php');
include('functions.php');


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/formstyles.css" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script type="text/javascript" src="js/Placeholders.js"></script>
		<script type="text/javascript">
			Placeholders.init({
			live: true,
			hideOnFocus: true});
		</script>
		<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
        <title><?php echo $_SESSION['username'];?>'s Messages</title>
    </head>
    <body>
	<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
        
<div id="carbonForm2">
<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
	<ul id="nav">
	<li><a href="page.php">Home</a></li>
	<li><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li class="current"><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (<?php echo checkMessages();?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
<?php

//We check if the user is logged
if(isset($_SESSION['username']))
{
$form = true;
$otitle = '';
$orecip = '';
$omessage = '';

if(isset($_POST['title'], $_POST['recip'], $_POST['message']))
{
        $otitle = $_POST['title'];
        $orecip = $_POST['recip'];
        $omessage = $_POST['message'];
        //We remove slashes depending on the configuration
        if(get_magic_quotes_gpc())
        {
                $otitle = stripslashes($otitle);
                $orecip = stripslashes($orecip);
                $omessage = stripslashes($omessage);
        }
        //We check if all the fields are filled
        if($_POST['title']!='' and $_POST['recip']!='' and $_POST['message']!='')
        {
                //We protect the variables
                $title = mysql_real_escape_string($otitle);
                $recip = mysql_real_escape_string($orecip);
				
                $message = mysql_real_escape_string(nl2br(htmlentities($omessage, ENT_QUOTES, 'UTF-8')));
                //We check if the recipient exists
				$query = mysql_query('select count(id) as recip, id as recipid, (select count(*) from pm) as npm from users where username="'.$recip.'"') or die ("fetch".mysql_error());
               
			   $dn1 = mysql_fetch_array($query) or die("fetch".mysql_error());
                if($dn1['recip']==1)
                {
                        //We check if the recipient is not the actual user
                        if($dn1['recipid']!=$_SESSION['id'])
                        {
                                $id = $dn1['npm']+1;
                                //We send the message
                                if(mysql_query('insert into pm (id, id2, title, user1, user2, message, timestamp, user1read, user2read)values("'.$id.'", "1", "'.$title.'", "'.$_SESSION['id'].'", "'.$dn1['recipid'].'", "'.$message.'", "'.time().'", "yes", "no")')or die ("insertdata".mysql_error()))
                                {
?>



	<div class="message">The message has successfully been sent.<br />
	<a href="messages.php">List of my Personal messages</a></div>
<?php
                                        $form = false;
                                }
                                else
                                {
                                        //Otherwise, we say that an error occured
                                        $error = 'An error occurred while sending the message';
                                }
                        }
                        else
                        {
                                //Otherwise, we say the user cannot send a message to himself
                                $error = 'You cannot send a message to yourself.';
                        }
                }
                else
                {
                        //Otherwise, we say the recipient does not exists
                        $error = 'The recipient does not exist.';
                }
        }
        else
        {
                //Otherwise, we say a field is empty
                $error = 'A field is empty. Please fill of the fields.';
        }
}
elseif(isset($_GET['recip']))
{
        //We get the username for the recipient if available
        $orecip = $_GET['recip'];
}
if($form)
{
//We display a message if necessary
if(isset($error))
{
        echo '<div class="message">'.$error.'</div>';
}
//We display the form
?>
<div class="content">
        <h1>New Personal Message</h1>
    <form action="newmsg.php" method="post">
                Please fill the following form to send a Personal message.<br />
		<div class="formRow">
        <div class="field">
        <input type="text" value="<?php echo htmlentities($otitle, ENT_QUOTES, 'UTF-8'); ?>" id="title" name="title" placeholder="Title" /><br />
		</div>
        </div>
		<div class="formRow">
        <div class="field">
        <input type="text" value="<?php echo htmlentities($orecip, ENT_QUOTES, 'UTF-8'); ?>" id="recip" name="recip" placeholder="Recipient (Username)"/><br />
		</div>
        </div>
		<div class="formRow">
        <div class="field">
        <textarea cols="40" rows="5" id="message" name="message" placeholder="Message"><?php echo htmlentities($omessage, ENT_QUOTES, 'UTF-8'); ?></textarea><br />
		</div>
        </div>
        <div class="formRow">
        <div class="field">
        <br/><input type="submit" id="submit" value="Send" />
		</div>
        </div>
    </form>
</div>
</div>
</div>
<?php
}
}
else
{
        echo '<div class="message">You must be logged to access this page.</div>';
}
?>
               
       </div> </body>
</html>