<?php
session_start();
include 'db_connection.php';
include 'functions.php';
if(!isset($_POST["submit"])){ //Accepts or rejects the form and sends back if rejected.
	$_SESSION["Message"] = "<p>Insert information first! Did not get enough information. Try again.</p>";
	header("Location:adddoc.php");
	}
unset($_POST["submit"]);
	$query = "select book.entryid,title,author,genre,publisher,isbn,type from book natural left outer join author_book, genre_book, publisher_book where book.entryid=author_book.entryid and book.entryid=genre_book.entryid and book.entryid=publisher_book.entryid and genre like '%{$_POST["genre"]}%' and author like '%{$_POST["author"]}%' and publisher like '%{$_POST["publisher"]}%' and title like '%{$_POST["title"]}%' and `type`  like '{$_POST["type"]}'";
	
$result = mysql_query($query) or die(mysql_error());
$result_table = "<table><form action=\"insert.php\" method=\"post\"><tr>
<td>Title</td>
<td>Author</td>
<td>Genre</td>
<td>Publisher</td>
<td>ISBN</td>
<td>Type</td>
<td><b>Choose</b></td>
</tr>
";
$count = 0;
$rows = array();
if(mysql_num_rows($result)>0){ //Processes entire searchresults
	while($row = mysql_fetch_array($result)){
		$title = $row['title'];
		$rows[] = $row;
		$result_table = $result_table."<tr><td>{$row['title']}</td>";
		$result_table = $result_table."<td>{$row['author']}</td>";
		$result_table = $result_table."<td>{$row['genre']} </td>";
		$result_table = $result_table."<td>{$row['publisher']} </td>";
		$result_table = $result_table."<td>{$row['isbn']}</td>";
		$result_table = $result_table."<td>{$row['type']}</td>";
		$result_table = $result_table."<td><input type=\"radio\" name=\"choice\" value=\"".$count."\"></input></tr>";
		$count+=1;

		}
	$result_table =  $result_table."<tr><td><select name=\"permission\">
	<option value=\"0\">Private</option>
	<option value=\"2\">Public</option>
	<option value=\"1\">Friends</option></select></td><td>
	<input type=\"submit\" name=\"submitpicked\"></td></tr></table></form>";
	$_SESSION["rows"] = $rows;
	$_SESSION["results"] = $result_table;
	}
else{ 
	$_SESSION["results"] = "<p>No titles were found for your query! We apologize!</p>";
}
$_SESSION["new_table"] = "<form action=\"insert.php\" method=\"post\"><table><tr>

<td>Title*</td>
<td><input type=\"text\" name=\"title\"></td>
</tr>
<tr>
<td>Author*</td>
<td><input type=\"text\" name=\"author\"></td>
</tr>
<tr>
<td>Genre</td>
<td><input type=\"text\" name=\"genre\"></td>
</tr>
<tr>
<td>Publisher</td>
<td><input type=\"text\" name=\"publisher\"></td>
</tr>
<tr>
<td>ISBN*</td>
<td><input type=\"text\" name=\"isbn\"></td>
</tr>
<tr>
<td>Type</td>
<td><select name=\"type\">
<option value=\"b\">Book</option>
<option value=\"e\">E-Book</option>
<option value=\"j\">Journal</option>
</td>
<td><select name=\"permission\">
<option value=\"0\">Private</option>
<option value=\"1\">Public</option>
<option value=\"2\">Friends</option></select></td>
</tr><tr><td><input type=\"submit\" name=\"submitnew\"></td></tr>
</table>
";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<script type="text/javascript" src="js/Placeholders.js"></script>
<script type="text/javascript">
	Placeholders.init({
    live: true,
    hideOnFocus: true});
</script>
<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>Add a new MyDoc</title>
</head>
<body>
<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
	<div id="carbonForm2">
	<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
		<ul id="nav">
	<li><a href="page.php">Home</a></li>
	<li class="current"><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (0)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
	<p><h1>Add new MyDoc</h2></p>
	<?php  // Here all the results are printed and this is where the user can choose between picking an already found result, or creating a new entry.
	echo "<p><h3>Select the title you want from the selection we have found for you!</h3></p>";
	echo "<p>".$_SESSION["results"]."</p>";
	echo "<p><h3>Or submit a new entry to MyBookBag and help contribute to our society!</h3></p>";
	echo "<p>".$_SESSION["new_table"]."</p>";
	?>
	</div>
	</div>
	</div>
</body>
</html>
