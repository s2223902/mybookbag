<?php
	ob_start();
    session_start();
	include('functions.php');
    if(!isset($_SESSION['username'])){
         header("Location: index.php");
    }
		$FriendForm = addFriendForm(); //Creates initial friendship request form. Refer to functions(addFriendForm()) for better documentation.

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<script type="text/javascript" src="js/Placeholders.js"></script>
<script type="text/javascript">
	Placeholders.init({
    live: true,
    hideOnFocus: true});
</script>
<title><?php echo $_SESSION['username'];?>'s Friend Requests</title>
</head>

<body>
<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
	<div id="carbonForm2">
	<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
		<ul id="nav">
	<li><a href="page.php">Home</a></li>
	<li><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li class="current"><a href="friends.php">My Friends</a>
		<ul>
		<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (<?php echo checkMessages();?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
	<h1>My Friends</h1>
	<h3>Add Friends</h3>
	<br>
	<hr>
	<br>
	<p>Enter the username and email of the user you wish to add to your friends list. 
	You will be notified when they accept your request</p>
	<br>
<p><?php echo $FriendForm; ?></p> <!--Shows the form and lets the user fill it in -->
<p>Go to pending <a href="requests.php">friend requests</a>
	</div>
	</div>
	</div>
</body>
</html>