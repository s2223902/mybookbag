<?php
session_start();
include('db_connection.php');
include('functions.php');
//We check if the user is logged
if(isset($_SESSION['username']))
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/formstyles.css" type="text/css" />
		       <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
				<script type="text/javascript" src="js/Placeholders.js"></script>
		<script type="text/javascript">
			Placeholders.init({
			live: true,
			hideOnFocus: true});
		</script>
        <title><?php echo $_SESSION['username'];?>'s Messages</title>
    </head>
    <body>
    	<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">    
<div id="carbonForm2">
<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
	<ul id="nav">
	<a href="page.php">Home</a></li>
	<li><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li class="current"><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (<?php echo checkMessages();?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
<?php

//We check if the ID of the discussion is defined
if(isset($_GET['id']))
{
$id = intval($_GET['id']);
//We get the title and the narators of the discussion
$req1 = mysql_query('select title, user1, user2 from pm where id="'.$id.'" and id2="1"') or die ("fetch1".mysql_error());
$dn1 = mysql_fetch_array($req1);
//We check if the discussion exists
if(mysql_num_rows($req1)==1)
{
//We check if the user have the right to read this discussion
if($dn1['user1']==$_SESSION['id'] or $dn1['user2']==$_SESSION['id'])
{
//The discussion will be placed in read messages
if($dn1['user1']==$_SESSION['id'])
{
        mysql_query('update pm set user1read="yes" where id="'.$id.'" and id2="1"') or die ("fetch2".mysql_error());
        $user_partic = 2;
}
else
{
        mysql_query('update pm set user2read="yes" where id="'.$id.'" and id2="1"') or die ("fetch3".mysql_error());
        $user_partic = 1;
}
//We get the list of the messages
$req2 = mysql_query('select pm.timestamp, pm.message, users.id as userid, users.username from pm, users where pm.id="'.$id.'" and users.id=pm.user1 order by pm.id2') or die ("fetch3".mysql_error());
//We check if the form has been sent
if(isset($_POST['message']) and $_POST['message']!='')
{
        $message = $_POST['message'];
        //We remove slashes depending on the configuration
        if(get_magic_quotes_gpc())
        {
                $message = stripslashes($message);
        }
        //We protect the variables
        $message = mysql_real_escape_string(nl2br(htmlentities($message, ENT_QUOTES, 'UTF-8')));
        //We send the message and we change the status of the discussion to unread for the recipient
        if(mysql_query('insert into pm (id, id2, title, user1, user2, message, timestamp, user1read, user2read)values
		("'.$id.'", 
		"'.(intval(mysql_num_rows($req2))+1).'", 
		"", 
		"'.$_SESSION['id'].'", 
			"6", 
		"'.$message.'", 
		"'.time().'", 
		"", 
		"")') 
		and mysql_query('update pm set user'.$user_partic.'read="yes" where id="'.$id.'" and id2="1"') or die ("fetch4".mysql_error()))
        {
?>
<div class="message">Your message has successfully been sent.<br />
<a href="readmsg.php?id=<?php echo $id; ?>">Go to the discussion</a></div>
<?php
        }
        else
        {
?>
<div class="message">An error occurred while sending the message.<br />
<a href="readmsg.php?id=<?php echo $id; ?>">Go to the discussion</a></div>
<?php
        }
}
else
{
//We display the messages
?>
<div class="content">
<h1><?php echo $dn1['title']; ?></h1>
<table class="messages_table">
        <tr>
        <th class="author">User</th>
        <th>Message</th>
    </tr>
<?php
while($dn2 = mysql_fetch_array($req2))
{
?>
        <tr>
        <td class="author center">

<br /><a href="profile.php?id=<?php echo $dn2['id']; ?>"><?php echo $dn2['username']; ?></a></td>
        <td class="left"><div class="date">Sent: <?php echo date('m/d/Y H:i:s' ,$dn2['timestamp']); ?></div>
        <?php echo $dn2['message']; ?></td>
    </tr>
<?php
}
//We display the reply form
?>
</table><br />
<h2>Reply</h2>
<div class="center">
    <form action="readmsg.php?id=<?php echo $id; ?>" method="post">
        <label for="message" class="center">Message</label><br />
        <textarea cols="40" rows="5" name="message" id="message"></textarea><br />
        <br /><input type="submit" id="submit" value="Send" /><br />
    </form>
</div>
</div>
<?php
}
}
else
{
        echo '<div class="message">You dont have the rights to access this page.</div>';
}
}
else
{
        echo '<div class="message">This discussion does not exists.</div>';
}
}
else
{
        echo '<div class="message">The discussion ID is not defined.</div>';
}
}
else
{
        echo '<div class="message">You must be logged to access this page.</div>';
}
?>
                <div class="foot"><a href="messages.php">Go to my Personal messages</a></div>
			</div>
			</div>
			</div>
        </body>
</html>