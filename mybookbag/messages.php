<?php
session_start();
include('db_connection.php');
include('functions.php');
//We check if the user is logged in
if(isset($_SESSION['username']))
{
//We list his messages in a table
//Two queries are executes, one for the unread messages and another for read messages
$req1 = mysql_query('select m1.id, m1.title, m1.timestamp, count(m2.id) as reps, users.id as userid, users.username from pm as m1, pm as m2,users where ((m1.user1="'.$_SESSION['id'].'" and m1.user1read="no" and users.id=m1.user2) or (m1.user2="'.$_SESSION['id'].'" and m1.user2read="no" and users.id=m1.user1)) and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc') or die ("fetch1:".mysql_error());
$req2 = mysql_query('select m1.id, m1.title, m1.timestamp, count(m2.id) as reps, users.id as userid, users.username from pm as m1, pm as m2,users where ((m1.user1="'.$_SESSION['id'].'" and m1.user1read="yes" and users.id=m1.user2) or (m1.user2="'.$_SESSION['id'].'" and m1.user2read="yes" and users.id=m1.user1)) and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc') or die ("fetch2:".mysql_error());
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="css/formstyles.css" type="text/css" />
		<script type="text/javascript" src="js/Placeholders.js"></script>
		<script type="text/javascript">
			Placeholders.init({
			live: true,
			hideOnFocus: true});
		</script>
		<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
        <title><?php echo $_SESSION['username'];?>'s Messages</title>
	</head>
<body>
		<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
        
<div id="carbonForm2">
<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
	<ul id="nav">
	<li><a href="page.php">Home</a></li>
	<li><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li class="current"><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (<?php echo checkMessages();?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	
	</ul>
	<div class="fieldContainer">
	<p>This is the list of your messages:</p><br/>
	<p><a href="newmsg.php" class="link_new_pm">New Message</a><br /></p>
	<h3>Unread Messages(<?php echo intval(mysql_num_rows($req1)); ?>):</h3>
	<table>
        <tr>
        <th class="title_cell">Title</th>
        <th>Nb. Replies</th>
        <th>Participant</th>
        <th>Date of creation</th>
    </tr>
<?php
//We display the list of unread messages
while($dn1 = mysql_fetch_array($req1))
{
?>
        <tr>
        <td class="left"><a href="readmsg.php?id=<?php echo $dn1['id']; ?>"><?php echo htmlentities($dn1['title'], ENT_QUOTES, 'UTF-8'); ?></a></td>
        <td><?php echo $dn1['reps']-1; ?></td>
        <td><?php echo htmlentities($dn1['username'], ENT_QUOTES, 'UTF-8'); ?></td>
        <td><?php echo date('Y/m/d H:i:s' ,$dn1['timestamp']); ?></td>
    </tr>
<?php
}
//If there is no unread message we notice it
if(intval(mysql_num_rows($req1))==0)
{
?>
        <tr>
        <td colspan="4" class="center">You have no unread message.</td>
    </tr>
<?php
}
?>
</table>
<br />
<h3>Read Messages(<?php echo intval(mysql_num_rows($req2)); ?>):</h3>
<table>
        <tr>
        <th class="title_cell">Title</th>
        <th>Nb. Replies</th>
        <th>Participant</th>
        <th>Date or creation</th>
    </tr>
<?php
//We display the list of read messages
while($dn2 = mysql_fetch_array($req2))
{
?>
        <tr>
        <td class="left"><a href="readmsg.php?id=<?php echo $dn2['id']; ?>"><?php echo htmlentities($dn2['title'], ENT_QUOTES, 'UTF-8'); ?></a></td>
        <td><?php echo $dn2['reps']-1; ?></td>
        <td><?php echo htmlentities($dn2['username'], ENT_QUOTES, 'UTF-8'); ?></td>
        <td><?php echo date('Y/m/d H:i:s' ,$dn2['timestamp']); ?></td>
    </tr>
<?php
}
//If there is no read message we notice it
if(intval(mysql_num_rows($req2))==0)
{
?>
        <tr>
        <td colspan="4" class="center">You have no read message.</td>
    </tr>
<?php
}
?>
</table>
</div>
</div>
<?php
}
else
{
        echo 'You must be logged to access this page.';
}
?>		
				</div>
                </div>
               
        </body>
</html>