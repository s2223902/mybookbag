<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<title>MyBookBag Confirmation</title>
</head>
<body>

<?php include ("php/random-bg.php"); ?>

<div id="carbonForm">
	<div id="logo">
		<img src="img/logo.png" />
	</div>

	<h1>Success!</h1>

    <div class="fieldContainer">
	<p>Thank you for registering! A confirmation email has been sent to your email address.</p><br />
	<p>Please click on the activation link to pick up your BookBag!</p><br />
	<p>Go to <a href="index.php">home</a></p>
    </div> <!-- Closing fieldContainer -->
    
</div>
    
</body>
</html>