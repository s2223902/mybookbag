<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
	ob_start();
    session_start();
	include('functions.php');
    if(!isset($_SESSION['username'])){
         header("Location: index.php");
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/Placeholders.js"></script>
<script type="text/javascript">
	Placeholders.init({
    live: true,
    hideOnFocus: true});
</script>
<title><?php echo $_SESSION['username'];?>'s Member Area </title>
</head>
<body>
<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
	<div id="carbonForm2">
	<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
	<ul id="nav">
	<li class="current"><a href="page.php">Home</a></li>
	<li><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (<?php echo checkMessages();?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="request.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
	<h1>Home</h1>
	<br><hr><br>

	<?php 
	$reports = array("MyBooks"=>books_Set("b"),"MyJournals"=>books_Set("j"), "MyEbooks"=>books_Set("e"));
	$keys = array_keys($reports);
	foreach($keys as $key){
		if($reports[$key]){
			echo "<p>You currently have <b>{$reports[$key]} {$key}</b> in your MyBookBag.</p><br/>";
			
			}
	//print_r($reports[$key]);
	//echo($reports[$key]);
	if(!$reports[$key]){ // I realise this is a wrong if-else construction since I just test if(true) and if(false) instead of if(true) and else, but it simply doesn't work in any other way, it would result in server error 500.
		echo "<p>Apparently, you haven't added any <b>{$key}</b> yet!<br/> Start using MyBookBag optimally immediately by adding some {$key} to your personal MyBookBag.</p><br/>";
		}
	}
	echo "<p>View the MyBookBag tab to browse your library or to add some new MyDocs! Enjoy!</p>";
	?>
	</div>
	</div>
	</div>
</body>
</html>
