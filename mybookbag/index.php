<?php
include ('database_connection.php');
if (isset($_POST['formsubmitted'])) {
    // Initialize a session:
session_start();
    $error = array();//this array will store all error messages
  

    if (empty($_POST['e-mail'])) {//if the email supplied is empty 
        $error[] = 'You forgot to enter  your Email ';
    } else {


        if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_POST['e-mail'])) {
           
            $Email = $_POST['e-mail'];
        } else {
             $error[] = 'Your EMail Address is invalid  ';
        }


    }


    if (empty($_POST['Password'])) {
        $error[] = 'Please Enter Your Password ';
    } else {
        $Password = $_POST['Password'];
    }


       if (empty($error))//if the array is empty , it means no error found
    { 

       
		$PasswordHash = md5($Password);
        $query_check_credentials = "SELECT * FROM users WHERE (Email='$Email' AND password='$PasswordHash') AND Activation IS NULL";
   
        

        $result_check_credentials = mysqli_query($dbc, $query_check_credentials);
        if(!$result_check_credentials){//If the QUery Failed 
            echo 'Query Failed ';
        }

        if (@mysqli_num_rows($result_check_credentials) == 1)//if Query is successfull 
        { // A match was made.

           


            $_SESSION = mysqli_fetch_array($result_check_credentials, MYSQLI_ASSOC);//Assign the result of this query to SESSION Global Variable
           
            header("Location: page.php");
          

        }else
        { 
            
            $msg_error= 'Either Your Account is inactive or Email address /Password is Incorrect';
        }

    }  else {
        
        

echo '<div class="errormsgbox"> <ol>';
        foreach ($error as $key => $values) {
            
            echo '	<li>'.$values.'</li>';


       
        }
        echo '</ol></div>';

    }
    
    
    if(isset($msg_error)){
        
        echo '<div class="warning">'.$msg_error.' </div>';
    }
    /// var_dump($error);
    mysqli_close($dbc);

} // End of the main Submit conditional.



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<script type="text/javascript" src="js/Placeholders.js"></script>
<script type="text/javascript">
	Placeholders.init({
    live: true,
    hideOnFocus: true});
</script>
<title>MyBookBag Login</title>
</head>

<body>

<?php include ("php/random-bg.php"); ?>

<div id="carbonForm">

	<div id="logo">
		<img src="img/logo.png" />
	</div>
		
	<h1>Login</h1>

	<form action="index.php" method="post" id="registration_form">

	<div class="fieldContainer">
		<p><span style="margin: 0 0 0 10;">Not yet a member? Register <a href="registration.php">here</a></span></p>
		<br />
		
		<div class="formRow">			
			<div class="field">
				<input type="text" name="e-mail" id="e-mail" placeholder="email address" />
			</div>
		</div>
		
		<div class="formRow">
			<div class="field">
				<input type="password" name="Password" id="Password" placeholder="password" />
			</div>
		</div>
	</div> <!-- Closing fieldContainer -->
	
	<div class="signupButton">
		<input type="submit" name="formsubmitted" id="submit" value="Login" />
	</div>
	
	</form>
		
</div>

</html>
