<?php
session_start();
include('db_connection.php');
include('functions.php');


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="css/formstyles.css" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<script type="text/javascript" src="js/Placeholders.js"></script>
		<script type="text/javascript">
			Placeholders.init({
			live: true,
			hideOnFocus: true});
		</script>
		<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
        <title><?php echo $_SESSION['username'];?>'s Settings</title>
    </head>
    <body>
	<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
        
<div id="carbonForm2">
<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
	<ul id="nav">
	<li><a href="page.php">Home</a></li>
	<li><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages (<?php echo checkMessages();?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li class="current"><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
	<h1>Settings</h1>
	<h3>Updating Password</h3>
	<br>
	<hr>
	<br>
	
<?php

//We check if the user is logged
if(isset($_SESSION['username']))
{
$form = true;
$opassword = '';
$opassword2 = '';
$opassword3 = '';

if(isset($_POST['password'], $_POST['password2'], $_POST['password3']))
{
        $opassword = $_POST['password'];
        $opassword2 = $_POST['password2'];
        $opassword3 = $_POST['password3'];
        //We remove slashes depending on the configuration
        if(get_magic_quotes_gpc())
        {
                $opassword = stripslashes($opassword);
                $opassword2 = stripslashes($opassword2);
                $opassword3 = stripslashes($opassword3);
        }
        //We check if all the fields are filled
        if($opassword2 == $opassword3)
        {
                //We protect the variables
                $password = mysql_real_escape_string($opassword);
                $password2 = mysql_real_escape_string($opassword2);
				$password3 = mysql_real_escape_string($opassword3);
                
                //We update the password
				$PasswordHash = md5($password);
				$PasswordHash3 = md5($password3);
				$query_change_credentials = "update `users` set `password`='$PasswordHash3' where `id`={$_SESSION["id"]} and `password`='$PasswordHash'";
				$result_check_credentials = mysql_query($query_change_credentials);
               
                if($result_check_credentials)
                {
?>



	<div class="message">The password has been succesfully updated<br />
	<a href="settings.php">Back to settings</a></div>
<?php
                                        $form = false;
                                }
                                else
                                {
                                        //Otherwise, we say that an error occured
                                        $error = 'An error occurred while updating password';
                                }
                        
                        
                }
                else
                {
                        //Otherwise, we say the passwords do not match
                        $error = 'The passwords do not match';
                }
        }
        else
        {
                //Otherwise, we say a field is empty
                $error = 'Please fill in the fields to update password';
        }


if($form)
{
//We display a message if necessary
if(isset($error))
{
        echo '<div class="message">'.$error.'</div>';
}
//We display the form
?>


	<form action="passreset.php" method="post" id="registration_form">

	
		<br />
		
		<div class="formRow">			
			<div class="field">
				<input type="password" name="password" id="password" placeholder="Current Password" />
			</div>
		</div>
		
		<div class="formRow">
			<div class="field">
				<input type="password" name="password2" id="password2" placeholder="New Password" />
			</div>
		</div>
			<div class="formRow">
			<div class="field">
				<input type="password" name="password3" id="password3" placeholder="Retype Password" />
			</div>
		</div>
		<br>
		Back to <a href="settings.php">settings</a>.
	</div> <!-- Closing fieldContainer -->
	
	<div class="signupButton">
		<input type="submit" name="formsubmitted" id="submit" value="Change Password" />
	</div>
	
	</form>
		
</div>

</div> 

<?php
}
}
else
{
        echo '<div class="message">You must be logged to access this page.</div>';
}
?>
          </div>     
       </body>
</html>