<?php
session_start();
include 'db_connection.php';
include 'functions.php';
if(!isset($_POST)){
	$_SESSION["Message"] = "<p>Something went wrong! Try again.</p>";
	header("Location:adddoc.php");
	}


if(isset($_POST["submitnew"])){ //Treats the given form (other page it is said that users can try and insert new data in two different ways. Refer to that for more explanation.
	if(empty($_POST["title"]) || empty($_POST["author"]) || empty($_POST["isbn"])){
		$_SESSION["Message"] = "<p>You didn't insert enough data for us to add your new entry!</p>";
		header("Location:adddoc.php");


		}
	else{
		$title = $_POST["title"];
		$author = $_POST["author"];
		$isbn = $_POST["isbn"];
		$genre = $_POST["genre"];
		$publisher = $_POST["publisher"];
		$type = $_POST["type"];
		$permission = intval($_POST["permission"]);
		$entryid = getid();
		}
	$queries = array(
		0 => "insert ignore into entry(`entryid`,`type`) values($entryid,'$type')",
		1 => "insert ignore into book(`title`,`entryid`,`isbn`,`type`) values('$title',$entryid,'$isbn','$type')",
		2 => "insert ignore into author(`name`,`birthyear`) values('$author','2010')",
		3 => "insert ignore into author_book(`author`,`entryid`) values('$author',$entryid)",
		4 => "insert ignore into publisher(`name`) values('$publisher')",
		5 => "insert ignore into publisher_book(`publisher`,`entryid`) values('$publisher',$entryid)",
		6 => "insert ignore into genre_book(`genre`,`entryid`) values('$genre',$entryid)",
		7 => "insert into `user_book`(`id`,`entryid`,`permission`) values('{$_SESSION["id"]}',$entryid,$permission)"
		);

	for($i=0;$i<count($queries);$i++){
		$query = mysql_real_escape_string($queries[$i]);
		$handle = mysql_query($queries[$i]) or die("Error at $query:".mysql_error());
		
		}
	$Msg = "Everything was added neatly!<br/> Go to MyLibrary to check out your new MyBook!";

	}
if(isset($_POST["submitpicked"])){
	$choice = intval($_POST["choice"]);
	$entryid = $_SESSION["rows"][$choice]["entryid"];
	$permission = $_POST["permission"];
	$id = getid();
	$query = "insert into user_book(`id`,`entryid`,`permission`) values({$_SESSION["id"]},$entryid,$permission)";
	$query = mysql_real_escape_string($query);
	$handle = mysql_query($query) or die("Error at $query:".mysql_error());
	$Msg = "Everything was added neatly!<br/> Go to MyLibrary to check out your new MyBook!";
	}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="css/formstyles.css" type="text/css" />
<script type="text/javascript" src="js/Placeholders.js"></script>
<script type="text/javascript">
	Placeholders.init({
    live: true,
    hideOnFocus: true});
</script>
<link href="css/toolbar.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <title>Add a new MyDoc</title>
</head>
<body>
<?php include ("php/random-bg.php"); ?>
<div id="mainContainer">
	<div id="carbonForm2">
	<div id="logo2">
		<img src="img/logo.png" />
		<p>Welcome <b><?php echo $_SESSION['username'];?></b></p>
	</div>
		<ul id="nav">
	<li><a href="page.php">Home</a></li>
	<li class="current"><a href="mybooks.php">MyBookBag</a>
		<ul>
			<li><a href="mybooks.php">My Books</a></li>
			<li><a href="myebooks.php">My eBooks</a></li>
			<li><a href="myjournals.php">My journals</a></li>
			<li><a href="adddoc.php">Add Books</a></li>
		</ul>
	</li>
	<li><a href="friends.php">My Friends</a>
		<ul>
			<li><a href="friends.php">My Friends</a></li>
			<li><a href="messages.php">Messages(<?php echo checkMessages(); ?>)</a></li>
			<li><a href="addfriend.php">Add friends</a></li>
			<li><a href="requests.php">Friend Requests</a></li>
		</ul>
	</li>
	
	<li><a href="settings.php">Settings</a>
		<ul>
		<li><a href="passreset.php">Change Password</a></li>
		</ul>
		</li>
	<li><a href="contact.php">Contact</a></li>
	<li><a href="logout.php">Log Out</a></li>
	</ul>
	<div class="fieldContainer">
	<p><h1>Add new MyDoc</h2></p><p>
<?php     
//Echo result message.
echo "<h2>$Msg</h2>";

//Delete these variables to avoid weird page refreshing issues
unset($_POST);
unset($_SESSION["rows"]);
?></p>
	</div>
	</div>
	</div>

</body>
</html>
